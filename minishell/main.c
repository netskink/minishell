//
//  main.c
//  minishell
//
//  Created by John Fred Davis on 3/16/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#include "unp.h"
#include "error.h"

int main(int argc, const char * argv[]) {
    
    char buf[MAXLINE];
    pid_t pid;
    int status;
    
    printf("%% "); // print prompt (printf requires %% to print %
    while (fgets(buf, MAXLINE, stdin) != NULL) {
        
        buf[strlen(buf) - 1] = 0;   // replace newline with null
        
        if ( (pid = fork()) < 0) {
            err_sys("fork error");
        } else if ( pid == 0) {
            // CHILD
            execlp(buf, buf, (char *) 0);
            err_ret("couldn't execute: %s", buf);
            exit(127);
        }
        
        // PARENT
        if ( (pid = waitpid(pid, &status, 0)) < 0) {
            err_sys("waitpid error");
        }
        
        // print new prompt
        printf("%% ");
    }
    
    
    return 0;
}
